import keras
from keras import layers
import numpy as np
import random
import sys
import re
import pandas as pd
import csv
import spacy
import tensorflow as tf
from tensorflow import keras
from langdetect import detect
from nltk.tokenize import sent_tokenize
import nltk
from nltk.corpus import wordnet as wn
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from keras.utils import np_utils
from keras import optimizers
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
from collections import Counter




def Get_Generated_Random_Review(train_data,weight_file): #only return well-formed reviews
    # Load Larger LSTM network and generate text
    # load ascii text and covert to lowercase
    filename = train_data#"reviews_training_gen2.txt"
    raw_text = open(filename).read()
    raw_text = raw_text.lower()
    # create mapping of unique chars to integers, and a reverse mapping
    chars = sorted(list(set(raw_text)))
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    int_to_char = dict((i, c) for i, c in enumerate(chars))
    # summarize the loaded data
    n_chars = len(raw_text)
    n_vocab = len(chars)
    print("Total Characters: ", n_chars)
    print("Total Vocab: ", n_vocab)
    # prepare the dataset of input to output pairs encoded as integers
    seq_length = 100
    dataX = []
    dataY = []
    for i in range(0, n_chars - seq_length, 1):
        seq_in = raw_text[i:i + seq_length]
        seq_out = raw_text[i + seq_length]
        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])
    n_patterns = len(dataX)
    #print("Total Patterns: ", n_patterns)
    #print(test)
    # reshape X to be [samples, time steps, features]
    X = np.reshape(dataX, (n_patterns, seq_length, 1))
    # normalize
    X = X / float(n_vocab)
    # one hot encode the output variable
    y = np_utils.to_categorical(dataY)
    # define the LSTM model
    model = Sequential()
    model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(256))
    model.add(Dropout(0.2))
    model.add(Dense(y.shape[1], activation='softmax'))
    # load the network weights
    file = weight_file#"weights-General2-59-0.0043-bigger.hdf5"
    model.load_weights(file)
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    # pick a random seed
    
    start = np.random.randint(0, len(dataX)-1)
    pattern = dataX[start]
    
    token=nltk.word_tokenize(raw_text)
    words = []
    for word in token:
        if "," not in word and "''" not in word and "``" not in word and "." not in word and "!" not in word and "?" not in word:
            words.append(word)
    #print("Seed:")
    #print("\"", ''.join([int_to_char[value] for value in pattern]), "\"")
    #print("---------------")
     #generate characters
    good_review = False
    good_con = False
    while good_review == False:
        review = ""
        result = ""
        r = ""
        arrayC = []
        check = True
        start = np.random.randint(0, len(dataX)-1)
        pattern = dataX[start]
        #print(dataX)
        for i in range(1000):#1000
            x = np.reshape(pattern, (1, len(pattern), 1))
            x = x / float(n_vocab)
            prediction = model.predict(x, verbose=0)
            index = np.argmax(prediction)
            result = int_to_char[index]
            seq_in = [int_to_char[value] for value in pattern]
            #sys.stdout.write(result)
            pattern.append(index)
            pattern = pattern[1:len(pattern)]
            r += result
        reviews = r.split("\n")
        for x,review in enumerate(reviews):   
            c = '\"\\'
            for char in c:
                arrayC.append(review.replace(char,""))
        final_review = []
        for s in arrayC[4:11:]:
            if '\"' not in s and '\\' not in s:
                #s = ''.join([i.capitalize() for i in s])
                if s not in final_review:
                    t = s + " "
                    final_review.append(t)
        #rtn = re.split('([.!?] *)',  final_review)
        final_review = ''.join([i.capitalize() for i in final_review])
        #print(array)
        #print(words)
        
        words_review = nltk.word_tokenize(final_review)
        for w in words_review:
            w = w.lower()
            #print(w)
            if w not in token:
                check = False
                #print("Failed")
                review = "Failed"
                #return "Failed"
        if check == True and review != "Failed":
            review = final_review
            #return final_review
        sen = nltk.sent_tokenize(review)
        print(len(sen))
        if len(sen) > 3:
            if sen[0] != sen[1] and sen[1] != sen[2] and sen[2] != sen[3] and sen[0] != sen[3] and sen[0] != sen[2] and sen[1] != sen[3]:
                good_con = True
            if review != "Failed" and good_con == True:
                good_review = True  
                return review
                print("\nDone.")
            else:
                print("Failed")
                print("failed review: " , final_review)
        else:
            print("Failed")
            print("failed review: " , final_review)
			
r = Get_Generated_Random_Review("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/review_pos_only_sen.txt","D:/3deJaar/Stage/AIreviewerClone/notebooks/weight_files/weights-pos-only-sen-44-0.0885.hdf5")
print(r)