import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ReviewService, Review } from '../review.service';
import { saveAs } from 'file-saver';
import { delay } from 'rxjs/operator/delay';
import { NgProgress } from 'ngx-progressbar';


@Component({
  selector: 'app-reviewer',
  templateUrl: './reviewer.component.html',
  styleUrls: ['./reviewer.component.scss']
})
export class ReviewerComponent {
  

 finalReview: Review;
 book: string; 
 titlebook: string = "Title book";
 toggle: boolean = false;


  constructor(private service: ReviewService,public ngProgress:NgProgress ) { }


 

  
   /* this.service.GetRandomReview("review_pos_only_sen","weights-pos-only-sen-44-0.0885").subscribe(
      (data:any) => {
        console.log(data)
       
      }
    )

    this.service.GetContextReview("Seq2Seq_final","TestHarry").subscribe(
      (data:any) => {
        console.log(data)
       
      }
    )*/
  

  GetReview(event){ 
  // console.log(this.book);

  if(this.titlebook != "Title book" && this.book){
  this.ngProgress.start();
  this.finalReview = null;
  var blob = new Blob([this.book], {type: "text/plain;charset=utf-8"});
  saveAs(blob,(this.titlebook + ".txt"));
  if(!this.toggle){
    setTimeout( () => {this.service.GetContextReview("Seq2Seq_final2", this.titlebook).subscribe(data => this.finalReview = data);}   , 500)   
  }
  if(this.toggle){
    //setTimeout( () => {this.service.GetRandomReview("review_pos_only_sen","weights-pos-only-sen-44-0.0885").subscribe(data => this.finalReview = data);}   , 500) 
   this.service.GetRandomReview("RandomReviewTrainingData","Final-Random-Reviews-42-0.1537").subscribe(data => this.finalReview = data); 
  }
 }
  else{
    alert("you forgot to fill in the title or forgot to fill in the text from te book")
  }
}

/*
 GetReview(event){ 
  this.service.GetContextReview("Seq2Seq_final", this.titlebook).subscribe(data => this.finalReview = data);
 }*/

 /*test(event){
  this.finalReview 
 }*/



}
