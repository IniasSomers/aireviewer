import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, ErrorHandler } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from "@angular/router";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ReviewerComponent } from './reviewer/reviewer.component';
import { ListComponent } from './list/list.component';
import { ReviewService } from './review.service';
import {HttpClientModule } from '@angular/common/http';
import { FormsModule} from "@angular/forms"
import { NgProgressModule, NgProgress } from 'ngx-progressbar'
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ReviewerPdfComponent } from './reviewer-pdf/reviewer-pdf.component';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    NavBarComponent,
    ReviewerComponent,
    ListComponent,
    ReviewerPdfComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgProgressModule,
    PdfViewerModule,
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent},
      { path: 'list', component: ListComponent},
      { path: 'reviewer', component: ReviewerComponent},
      { path: 'reviewerPdf', component: ReviewerPdfComponent},
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: "**", component: PageNotFoundComponent}
    ], { useHash: true })
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [
    ReviewService,
    NgProgress,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
