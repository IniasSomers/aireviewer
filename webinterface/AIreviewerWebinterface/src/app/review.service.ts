import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map' ;
import { catchError } from 'rxjs/operators';
import { pipe } from 'rxjs/Rx';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { NgProgress } from 'ngx-progressbar';


@Injectable()
export class ReviewService {

// http://localhost:5000/random/review_pos_only_sen.txt/weights-pos-only-sen-44-0.0885.hdf5
// http://localhost:5000/context/Seq2Seq_final.txt/The-Return-of-Tarzan.txt/TEST_TEST3

  constructor(private http: HttpClient,public ngProgress: NgProgress) { }

  GetRandomReview(trainingSet:string , modelWeights:string): Observable<Review>{
    return this.http.get<Review>(`http://localhost:5000/random/${trainingSet}.txt/${modelWeights}.hdf5`)
    .pipe(catchError(this.handleError));
  }

  GetContextReview(trainingSet:string , bookName:string): Observable<Review>{
    return this.http.get<Review>(`http://localhost:5000/context/${trainingSet}.txt/${bookName}.txt/FinalM`)
    .pipe(catchError(this.handleError));
  }
 

  private handleError(errorResponse:HttpErrorResponse){
    if(errorResponse.error instanceof ErrorEvent){
      console.log("client error:" , errorResponse.error.message)
      alert("There was an client error!");
      window.location.reload();
    }
    else{
      alert("Server error! There was something wrong with the text of the book or the title..");
      window.location.reload();
      //this.ngProgress.done();
    }
    return new ErrorObservable("Error occurred!!")
  }




}

export interface Book {
  id: string;
  title: string; 
  imgUrl: string;
  review: string;
}


export interface Review {
  text: string;
}