import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { Review, ReviewService } from '../review.service';
import { NgProgress } from 'ngx-progressbar';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-reviewer-pdf',
  templateUrl: './reviewer-pdf.component.html',
  styleUrls: ['./reviewer-pdf.component.scss']
})
export class ReviewerPdfComponent {

  finalReview: Review;
  book: string; 
  titlebook: string = "Title book";
  toggle: boolean = false;
  pdfSrc:string = '';
  allText:string[] = [];
  disabled:boolean = false;

  constructor(private service: ReviewService,public ngProgress:NgProgress) { }

 

// pdf gedeelte

textLayerRendered(e: CustomEvent) {
  //console.log('(text-layer-rendered)', e.srcElement.textContent);//.srcElement.firstElementChild.textContent);
 // this.allText.push(e.srcElement.textContent);
 this.book += e.srcElement.textContent;
}

onFileSelected() {
  let img: any = document.querySelector("#file");

  if(typeof (FileReader) !== 'undefined') {
    let reader = new FileReader();

    reader.onload = (e:any) => {
      this.pdfSrc = e.target.result;
    }

    reader.readAsArrayBuffer(img.files[0]);
   // console.log(reader.readAsArrayBuffer())
  }
}

// get review
GetReview(event){ 
  // console.log(this.book);
  if(this.titlebook != "Title book" && this.book){
  this.ngProgress.start();
  this.finalReview = null;
  var blob = new Blob([this.book], {type: "text/plain;charset=utf-8"});
  saveAs(blob,(this.titlebook + ".txt"));
  if(!this.toggle){
    setTimeout( () => {this.service.GetContextReview("Seq2Seq_final2", this.titlebook).subscribe(data => this.finalReview = data);}   , 500)   
  }
  if(this.toggle){
    //setTimeout( () => {this.service.GetRandomReview("review_pos_only_sen","weights-pos-only-sen-44-0.0885").subscribe(data => this.finalReview = data);}   , 500) 
    setTimeout( () => {this.service.GetRandomReview("RandomReviewTrainingData","Final-Random-Reviews-42-0.1537").subscribe(data => this.finalReview = data);}   , 500)  
   }
  }
  else{
    alert("you forgot to fill in the title or forgot to upload an book (pdf)")
  }
 }



}
