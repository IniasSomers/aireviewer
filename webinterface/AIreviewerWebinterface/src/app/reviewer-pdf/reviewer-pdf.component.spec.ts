import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewerPdfComponent } from './reviewer-pdf.component';

describe('ReviewerPdfComponent', () => {
  let component: ReviewerPdfComponent;
  let fixture: ComponentFixture<ReviewerPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewerPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewerPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
