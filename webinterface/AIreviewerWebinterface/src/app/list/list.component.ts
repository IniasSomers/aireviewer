import { Component, OnInit } from '@angular/core';
import { ReviewService, Review ,Book} from '../review.service';
import { NgProgress } from 'ngx-progressbar';



@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  finalReview: Review;
  pdfSrc:string = '';
  allText:string[] = [];
  books:Book[] = [
    {
       id:'1',
       title: "\"Abe\" Lincoln's Anecdotes and Stories",
       imgUrl: "https://archive.org/download/abelincolnsanecd47811gut/47811-h/images/cover.jpg",
       review:"A good informative book is always a good one! The information about Lincoln was really great. I should state this was a fascinating book. I give it 5 stars!",
    },
    {
      id:'2',
      title: "\"Broke\" The Man Without the Dime",
      imgUrl: "https://archive.org/download/brokethemanwitho45412gut/45412-h/images/cover.jpg",
      review:"I really liked this thriller book. Jack was a real crazy mastermind. Cleveland is the place off all greatness. It was really nice to have the Big Four Station in the story. I loved it! I literally could not put the book down.",
   },
   {
    id:'3',
    title: "\"I was there\" with the Yanks in France.",
    imgUrl: "https://archive.org/download/quotiwastherequo15937gut/15937-h/images/image001.jpg",
    review:"I really liked this thriller book. Buddy was mind boggling in this story. I loved there was a part about Paris. The War was a significant piece of the book. I have fallen in love with this story!",
 },
 {
  id:'4',
  title: "The Monk Who Sold His Ferrari",
  imgUrl: "https://ia802309.us.archive.org/BookReader/BookReaderImages.php?zip=/32/items/TheMonkWhoSoldHisFerrariRobinS.Sharma/eBook_The%20Monk%20Who%20Sold%20His%20Ferrari_Robin%20S.%20Sharma_jp2.zip&file=eBook_The%20Monk%20Who%20Sold%20His%20Ferrari_Robin%20S.%20Sharma_jp2/eBook_The%20Monk%20Who%20Sold%20His%20Ferrari_Robin%20S.%20Sharma_0000.jp2&scale=2&rotate=0",
  review:"I adore adventure books! John was actually a great person! That the story was in Sivana made it a bit different. the Great Sages of Sivana in the story was a great part. I loved it! I literally could not put the book down.",
},
{
  id:'5',
  title: "Fundamentals Of Electric Circuits",
  imgUrl: "https://ia802507.us.archive.org/BookReader/BookReaderImages.php?zip=/23/items/FundamentalsOfElectricCircuits5thEdgnv64_201406/Fundamentals%20of%20Electric%20Circuits%20%285th%20Ed%29%28gnv64%29_jp2.zip&file=Fundamentals%20of%20Electric%20Circuits%20%285th%20Ed%29%28gnv64%29_jp2/Fundamentals%20of%20Electric%20Circuits%20%285th%20Ed%29%28gnv64%29_0000.jp2&scale=4&rotate=0",
  review:"Informative books i adore it! The information about PSpice was really great. I should state this was a fascinating book. I give it 5 stars!",
},
{
  id:'6',
  title: "The Silmarillion",
  imgUrl: "https://ia800605.us.archive.org/BookReader/BookReaderImages.php?zip=/29/items/fegmcfeggerson_gmail_4731/473%20%281%29_jp2.zip&file=473%20%281%29_jp2/473%20%281%29_0000.jp2&scale=2&rotate=0",
  review:"It was a very nice story that i loved to read. I've read it so many times and never has it grown old. I love how it shifts views and tells you what other people are doing and how it has many plots that all tie together in the book. I give it 5 stars!",
},
{
  id:'7',
  title: "Cleopatra's Needle",
  imgUrl: "http://www.gutenberg.org/cache/epub/37785/pg37785.cover.medium.jpg",
  review:"I loved this book, read it in one sitting in light of the fact that the story keeped me pondering. You know how a book comes into your life just at the right time, well this is the book for me. I have fallen in love with this story! It was also very realistic!",
},
{
  id:'8',
  title: "Molecular genetics of bacteria",
  imgUrl: "https://ia802707.us.archive.org/BookReader/BookReaderImages.php?zip=/31/items/MolecularGeneticsOfBacteria/Larry_Snyder_Wendy_Champness-Molecular_Genetics_of_Bacteria_Third_Edition__-ASM_Press2007_jp2.zip&file=Larry_Snyder_Wendy_Champness-Molecular_Genetics_of_Bacteria_Third_Edition__-ASM_Press2007_jp2/Larry_Snyder_Wendy_Champness-Molecular_Genetics_of_Bacteria_Third_Edition__-ASM_Press2007_0000.jp2&scale=8&rotate=0",
  review:"I love this kind of informative books. It was interesting to learn more about RNA. This was actually an really interesting book. 5 stars!",
},
{
  id:'9',
  title: "Destiny Distrupted History Of The World Through Islamic Eyes Tamim Ansary",
  imgUrl: "https://ia601306.us.archive.org/BookReader/BookReaderImages.php?zip=/5/items/DestinyDistruptedHistoryOfTheWorldThroughIslamicEyesTamimAnsary/destiny%20distrupted%20-%20history%20of%20the%20world%20through%20islamic%20eyes%20-%20tamim%20ansary_jp2.zip&file=destiny%20distrupted%20-%20history%20of%20the%20world%20through%20islamic%20eyes%20-%20tamim%20ansary_jp2/destiny%20distrupted%20-%20history%20of%20the%20world%20through%20islamic%20eyes%20-%20tamim%20ansary_0000.jp2&scale=8&rotate=0",
  review:"Informative books i love it! The information about Islam was really great. This is a really great book to learn from.",
},
{
  id:'10',
  title: "Dracula",
  imgUrl: "https://images-na.ssl-images-amazon.com/images/I/51Z040Fzy3L._SX321_BO1,204,203,200_.jpg",
  review:"I truly adore this frightening story. Lucy was extremely extraordinary in the story! She has an special personality. That the story was in London made it a bit different. It was really nice to have the Honourable Arthur Holmwood in the story. I give it 5 stars! what an awesome book.",
},
{
  id:'11',
  title: "Principle Of Neurology",
  imgUrl: "https://ia801206.us.archive.org/BookReader/BookReaderImages.php?zip=/17/items/AdamsAndVictorsPrincipleOfNeurology10thEd.PDFtahir99VRG/Adams%20and%20Victor%27s%20-%20Principle%20of%20Neurology%2010th%20Ed.%20%5BPDF%5D%5Btahir99%5D%20VRG_jp2.tar&file=Adams%20and%20Victor%27s%20-%20Principle%20of%20Neurology%2010th%20Ed.%20%5BPDF%5D%5Btahir99%5D%20VRG_jp2/Adams%20and%20Victor%27s%20-%20Principle%20of%20Neurology%2010th%20Ed.%20%5BPDF%5D%5Btahir99%5D%20VRG_0000.jp2&scale=8&rotate=0",
  review:"I love this kind of informative books. Wow the info about CSF was really good. This is a good start if you want to know more about this.",
},
{
  id:'12',
  title: "Percy Jackson's Greek Gods",
  imgUrl: "https://archive.org/services/img/PercyJacksonsGreekGods",
  review:"What an adventure! Zeus was mind boggling in this story. I love that the story was in Titan. It was really nice to have the Trojan War in the story. I have fallen in love with this story!",
},
{
  id:'13',
  title: "Blackbeard",
  imgUrl: "https://images-na.ssl-images-amazon.com/images/I/413dhYJErnL._SX331_BO1,204,203,200_.jpg",
  review:"Wow scary story! Blackbeard is simply very cool! England is a little nation but the general population are extraordinary. The end was super sad. Overall this books gets 5 stars!",
},
{
  id:'14',
  title: "Introduction To Operations Research",
  imgUrl: "https://ia801605.us.archive.org/BookReader/BookReaderImages.php?zip=/24/items/IntroductionToOperationsResearch10thEd2015/Introduction%20to%20Operations%20Research%2010th%20Ed%20%5B2015%5D_jp2.zip&file=Introduction%20to%20Operations%20Research%2010th%20Ed%20%5B2015%5D_jp2/Introduction%20to%20Operations%20Research%2010th%20Ed%20%5B2015%5D_0000.jp2&scale=4&rotate=0",
  review:"A good informative book is always a good one! It was interesting to learn more about CPF. I must say that this was an interesting book. It deserves 5 stars!",
},
  ];

  

  constructor(private service: ReviewService,public ngProgress:NgProgress ) { }

  ngOnInit() {
  
    //this.ngProgress.start();
  }
  






}
