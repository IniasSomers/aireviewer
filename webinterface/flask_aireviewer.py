import keras
from keras import layers
import numpy as np
import random
import sys
import re
import pandas as pd
import csv
import spacy
import tensorflow as tf
from tensorflow import keras
from langdetect import detect
from nltk.tokenize import sent_tokenize
import nltk
from nltk.corpus import wordnet as wn
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from keras.utils import np_utils
from keras import optimizers
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
from collections import Counter
from flask import Flask
from flask_cors import CORS
from flask import jsonify
app = Flask(__name__)
CORS(app)

@app.route('/random/<data>/<weight>')
def GetRandomReview(data,weight):
	trainingdata = "D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/" + data
	weightfile = "D:/3deJaar/Stage/AIreviewerClone/notebooks/weight_files/" + weight
	review = Get_Generated_Random_Review(trainingdata,weightfile)
	return jsonify(review)
	
@app.route('/context/<data>/<book>/<modelname>')
def GetContextReview(data,book,modelname):
	trainingdata = "D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/" + data
	review = GetBookReview(trainingdata,book,modelname)
	return jsonify(review)

def GetBookItemAll(book,tag): # Get tag out of the book text (bv persons)
    Context = []  
    nlp = spacy.load('en_core_web_sm')
    text = nlp(state_union.raw(book))
    array = []
    for word in text.ents:
        if tag in word.label_:
            array.append(word.text)
    MostUsed = Counter(array)
    #print(MostUsed)
    for w in MostUsed:
        word = str(w).split("',")
        Context.append(word[0][0:])
    return Context
   # return(Counter(array).most_common(4))	
   
def GetBookItem(book,tag):
    Context = []
    nlp = spacy.load('en_core_web_sm')
    try:
        f = open("D:/Users/Inias Somers/Downloads/" + book, "r")
        booktext = f.read()
    except:
        f = open("D:/Users/Inias Somers/Downloads/" + book, "r",encoding="utf8")
        booktext = f.read()
    if len(booktext) < 1000000:
         text = nlp(booktext)
    if len(booktext) > 1000000:
        text = nlp(booktext[:999999])
    array = []
    for word in text.ents:
        if tag in word.label_:
            array.append(word.text)
    MostUsed = Counter(array).most_common(4)
    #print(MostUsed)
    for w in MostUsed:
        word = str(w).split("',")
        Context.append(word[0][2:])
    #print(Context[0])
    if len(Context) != 0 and Context:
        if "\n" in Context[0] or "\\n" in Context[0]:
            c_words = nltk.word_tokenize(Context[0])
            unWanted = ["\n","\\n"]
            for x,w in enumerate(c_words):
                for c in unWanted:
                    c_words[x] = w.replace(c,"")
            final_list = []
            for x,w in enumerate(c_words):
                word = w.lower()
                if "a" in word or "e" in word or "i" in word or "j" in word or "o" in word or "u" in word or "y" in word:
                     final_list.append(w)
            Context[0] = ""
            for w in final_list:
                Context[0] = Context[0] + w + " "
            Context[0] = Context[0][:-1]
        if Context[0] == "ENGLISH":
             Context[0] = Context[1]
        if '"' in Context[0]:
            Context[0] = Context[0].replace('"',"")
    #if '\u' in Context[0].encode('utf8'):
     #   Context[0] = Context[0].replace('\u',"")
        if len(Context) > 1:
            if Context[1] and "." in Context[0]:
                Context[0] = Context[1]
            if Context[0] == "CHAPTER":
                Context[0] = Context[1]
        try:
            print(Context[0])
        except:
            print((Context[0]).encode('utf8')) 
            if len(Context) > 1:
                Context[0] = Context[1]
                if "\\u" in Context[0]:
                    Context[0] = "none"
                else:
                    print(Context[0])
    #print((Context[0]).encode('utf8'))  
    return Context
   
def GetBookItem2(book,tag):
    Context = []
    nlp = spacy.load('en_core_web_sm')
    try:
        f = open("D:/Users/Inias Somers/Downloads/" + book, "r")
        booktext = f.read()
    except:
        f = open("D:/Users/Inias Somers/Downloads/" + book, "r",encoding="utf8")
        booktext = f.read()
    if len(booktext) < 1000000:
         text = nlp(booktext)
    if len(booktext) > 1000000:
        text = nlp(booktext[:999999])
    array = []
    for word in text.ents:
        if tag in word.label_:
            array.append(word.text)
    MostUsed = Counter(array).most_common(4)
    #print(MostUsed)
    for w in MostUsed:
        word = str(w).split("',")
        Context.append(word[0][2:])
    print(Context[0])
    return Context
   # return(Counter(array).most_common(4))
   
#Check similarity
def CheckSimilarity(word,voc):
    nlp = spacy.load('en_core_web_sm')
    c = nlp(word)
    array = []
    for x,w in enumerate(voc):
        check = nlp(w)
        array.append(c.similarity(check))
    max_value = max(array)
    if max_value in array:
        index = array.index(max_value)
    print(array)
    return voc[index]

def GetVoc(book):
    array = []
    f = open(book, "r")
    array = f.read().split(",")
    for x,a in enumerate(array):
        array[x] = a.lower()
    return array

def GetGenre(book):
    genre=""
    thrillerVoc = GetVoc("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/ThrillerVoc.txt")
    horrorVoc = GetVoc("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/HorrorVoc.txt")
    adventureVoc = GetVoc("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/AdventureVoc.txt")
    sciencefictionVoc = GetVoc("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/ScienceFictionVoc.txt")
    fantasyVoc = GetVoc("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/FantasyVoc.txt")
    romanceVoc = GetVoc("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/RomanceVoc.txt")
    comedyVoc= GetVoc("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/ComedyVoc.txt")
    infoVoc= GetVoc("D:/3deJaar/Stage/AIreviewerClone/notebooks/training_data/InfoVoc.txt")
    location = "D:/Users/Inias Somers/Downloads/" + book
    try:
        f = open(location,"r")
        tekst = f.read()
    except:
        f = open(location,"r",encoding="utf8")
        tekst = f.read()
    bookVoc = nltk.word_tokenize(tekst)
    t=h=a=s=f=r=c=i=0
    for word in bookVoc:
        if word in thrillerVoc:
            t += 1
        if word in horrorVoc:
            h += 1
        if word in adventureVoc:
            a += 1
        if word in sciencefictionVoc:
            s += 1
        if word in fantasyVoc:
            f += 1
        if word in romanceVoc:
            r += 1
        if word in comedyVoc:
            c += 1
        if word in infoVoc:
            i += 1  
    print(t,h,a,s,f,r,c,i)
    m = max(t,h,a,s,f,r,c,i)
    if t == m:
        genre = "thriller"
    if h == m:
        genre = "horror"
    if a == m:
        genre = "adventure"
    if f == m:
        genre = "fantasy"
    if s == m:
        genre = "science fiction"
    if r == m:
        genre = "romance"
    if c == m:
        genre = "comedy"
    if i== m:
        genre = "informative"
    print(genre)
    return genre

def GetContext(book): # Get context keywords 
    
    genre = GetGenre(book)
    
    book_characters = []
    book_places = []
    book_events = []
    
    book_characters = GetBookItem(book,"PERSON")
    book_places = GetBookItem(book,"GPE")
    book_events = GetBookItem(book,"EVENT")
    
    if book_places == []:
        book_places = GetBookItem(book,"LOC")
    if book_places == []:
        book_places = GetBookItem(book,"PRODUCT")	
    if book_events == []:
        return genre , book_characters[0] , book_places[0] , "none"
    else:
        return genre , book_characters[0]  , book_places[0] , book_events[0]

def GetData(dataset): # alle nodige data uit dataset
    keywords = []
    answers = []
    genres = []
    persons = []
    places = []
    events = [] 
    endings = []
    check = 0
    f = open(dataset, "r")
    for x in f:
        if x[:-1] == "1":
            check = 1
        if x[:-1] == "2":
            check = 2
        if x[:-1] == "3":
            check = 3
        if x[:-1] == "4":
            check = 4
        if len(x) > 2:
            s = x.split(",")
            answers.append(s[1][:-1])
            keywords.append(s[0])
            if check == 0:
                genres.append(s[0][:-1])
            if check == 1:
                persons.append(s[0][:-1])
            if check == 2:
                places.append(s[0][:-1])
            if check == 3:
                events.append(s[0][:-1])
            if check == 4:
                endings.append(s[0][:-1])
    
    return keywords , answers , genres , persons , places , events , endings

def GetSen(tag_voc,senteces): # Get alle sentence where that items comes 
    array = []
    for r in tag_voc:
        for s in senteces:
            if r in s:
                if s not in array:
                    array.append(s)
    return array
   
def GetAnswer(context,chatbot,genres,persons,places,events,endings): # resultaat terug krijgen van trained model
    chatbot = ChatBot(chatbot)
    
    response = chatbot.get_response(context)
    return response

# contextuele review 
def GetReview(book,chatbot,genres,persons,places,events,endings,contextGenre,contextPerson,contextPlace,contextEvent="none",contextEnd="end"):
    chatbot = ChatBot(chatbot)
    context = [contextGenre,contextPerson,contextPlace,contextEvent,contextEnd]
    contextLists = [genres,persons,places,events,endings]
    review=""
    if contextGenre != "informative":
        for c,x in enumerate(context):
            if x != "none":
                if contextLists[c].count(x) != 1 and contextLists[c].count(x) > 1:
                    context[c] = x + str(random.randint(1,contextLists[c].count(x)))
                    review = review + " " + str(chatbot.get_response(context[c]))
                if contextLists[c].count(x) == 1:
                    context[c] = x + "1"
                    review = review + " " + str(chatbot.get_response(context[c]))
                if contextLists[c].count(x) == 0:
                    r = CheckSimilarity(x,contextLists[c])
                    if contextLists[c].count(r) != 1 and contextLists[c].count(r) > 1:
                        re = r + str(random.randint(1,contextLists[c].count(r)))
                        answer = str(chatbot.get_response(re))
                        answer = answer.replace(r , x ,1)
                        review = review + " " + answer
                    if contextLists[c].count(r) == 1:
                        re = r + "1"
                        answer = str(chatbot.get_response(re))
                        answer = answer.replace(r , x ,1)
                        review = review + " " + answer
                    if r in review:
                        review = review.replace(r,contextEvent) 
     
    if contextGenre == "informative":
        book_sub = []
        review = ""
        genre = "informative" + str(random.randint(1,contextLists[0].count("informative")))
        review = review + " " + str(chatbot.get_response(genre))
        book_sub =  GetBookItem(book,"ORG")
        if book_sub == []:
            book_sub = GetBookItem(book,"MISC")
        if book_sub == []:
            book_sub = GetBookItem(book,"PRODUCT")
        answer = str(chatbot.get_response("info" + str(random.randint(1,contextLists[1].count("info")))))
        answer = answer.replace("default" , book_sub[0] ,1)
        review = review + " " + answer
        answer2 = str(chatbot.get_response("infoEnd" + str(random.randint(1,contextLists[4].count("infoEnd"))))) 
        #answer2 = answer2.replace("default" , book_sub[0] ,1)
        review = review + " " + answer2	 
    #contextGenre = contextGenre + str(random.randint(1,genres.count(contextGenre)))
    #contextPerson = contextPerson  + str(random.randint(1,persons.count(contextPerson )))
    #contextPlace = contextPlace + str(random.randint(1,places.count(contextPlace)))
    #contextEvent = contextEvent + str(random.randint(1,events.count(contextEvent)))
    #contextEnd = contextEnd + str(random.randint(1,endings.count(contextEnd)))
    #review = ""
    #review = review + str(bot.get_response(contextGenre))
    #review = review + str(bot.get_response(contextPerson))
    #review = review + str(bot.get_response(contextPlace))
    #review = review + str(bot.get_response(contextEvent))
    #review = review + str(bot.get_response(contextEnd))
    print(contextGenre ,contextPerson , contextPlace, contextEvent,contextEnd )
    #print(context)
    return review
   
def GetBookReview(trainingdata,book,modelname):
    keywords , answers , genres , persons , places , events , endings = GetData(trainingdata)
    try:
        g,p,l,e = GetContext(book) 
        review = GetReview(book,modelname,genres,persons,places,events,endings,g,p,l,e)
        return review
    except:
        g,p,l = GetContext(book)
        review = GetReview(book,modelname,genres,persons,places,events,endings,g,p,l)
        return review   
 
def Get_Generated_Random_Review(train_data,weight_file): #only return well-formed reviews
    # Load Larger LSTM network and generate text
    # load ascii text and covert to lowercase
    filename = train_data#"reviews_training_gen2.txt"
    raw_text = open(filename).read()
    raw_text = raw_text.lower()
    # create mapping of unique chars to integers, and a reverse mapping
    chars = sorted(list(set(raw_text)))
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    int_to_char = dict((i, c) for i, c in enumerate(chars))
    # summarize the loaded data
    n_chars = len(raw_text)
    n_vocab = len(chars)
    print("Total Characters: ", n_chars)
    print("Total Vocab: ", n_vocab)
    # prepare the dataset of input to output pairs encoded as integers
    seq_length = 100
    dataX = []
    dataY = []
    for i in range(0, n_chars - seq_length, 1):
        seq_in = raw_text[i:i + seq_length]
        seq_out = raw_text[i + seq_length]
        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])
    n_patterns = len(dataX)
    print("Total Patterns: ", n_patterns)
    #print(test)
    # reshape X to be [samples, time steps, features]
    X = np.reshape(dataX, (n_patterns, seq_length, 1))
    # normalize
    X = X / float(n_vocab)
    # one hot encode the output variable
    y = np_utils.to_categorical(dataY)
    # define the LSTM model
    try:
        model = Sequential()
        model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
        model.add(Dropout(0.2))
        model.add(LSTM(256))
        model.add(Dropout(0.2))
        model.add(Dense(y.shape[1], activation='softmax'))
    # load the network weights
        file = weight_file#"weights-General2-59-0.0043-bigger.hdf5"
        model.load_weights(file)
        model.compile(loss='categorical_crossentropy', optimizer='adam')
    except:
        print("problem fix")
    # pick a random seed
    
    start = np.random.randint(0, len(dataX)-1)
    pattern = dataX[start]
    
    token=nltk.word_tokenize(raw_text)
    words = []
    for word in token:
        if "," not in word and "''" not in word and "``" not in word and "." not in word and "!" not in word and "?" not in word:
            words.append(word)
    #print("Seed:")
    #print("\"", ''.join([int_to_char[value] for value in pattern]), "\"")
    #print("---------------")
     #generate characters
    good_review = False
    good_con = False
    while good_review == False:
        review = ""
        result = ""
        r = ""
        arrayC = []
        check = True
        start = np.random.randint(0, len(dataX)-1)
        pattern = dataX[start]
        #print(dataX)
        for i in range(1000):#1000
            x = np.reshape(pattern, (1, len(pattern), 1))
            x = x / float(n_vocab)
            prediction = model.predict(x, verbose=0)
            index = np.argmax(prediction)
            result = int_to_char[index]
            seq_in = [int_to_char[value] for value in pattern]
            #sys.stdout.write(result)
            pattern.append(index)
            pattern = pattern[1:len(pattern)]
            r += result
        reviews = r.split("\n")
        for x,review in enumerate(reviews):   
            c = '\"\\'
            for char in c:
                arrayC.append(review.replace(char,""))
        final_review = []
        for s in arrayC[4:11:]:
            if '\"' not in s and '\\' not in s:
                #s = ''.join([i.capitalize() for i in s])
                if s not in final_review:
                    t = s + " "
                    final_review.append(t)
        #rtn = re.split('([.!?] *)',  final_review)
        final_review = ''.join([i.capitalize() for i in final_review])
        #print(array)
        #print(words)
        
        words_review = nltk.word_tokenize(final_review)
        for w in words_review:
            w = w.lower()
            #print(w)
            if w not in token:
                check = False
                #print("Failed")
                review = "Failed"
                #return "Failed"
        if check == True and review != "Failed":
            review = final_review
            #return final_review
        sen = nltk.sent_tokenize(review)
        print(len(sen))
        if len(sen) > 3:
            if sen[0] != sen[1] and sen[1] != sen[2] and sen[2] != sen[3] and sen[0] != sen[3] and sen[0] != sen[2] and sen[1] != sen[3]:
                good_con = True
            if review != "Failed" and good_con == True:
                good_review = True  
                return review
                print("\nDone.")
            else:
                print("Failed")
                print("failed review: " , final_review)
        else:
            print("Failed")
            print("failed review: " , final_review)
#print("\nDone.")


#Get_Generated_Random_Review("training_data/review_pos_only_sen.txt","weight_files/weights-pos-only-sen-44-0.0885.hdf5")